package main;

import java.io.IOException;
import java.util.Random;

public class Main {
	public static int findKthLargest(int[] nums, int k) {
		int start = 0, end = nums.length - 1, index = k;

		while (start < end) {
			int pivot = partion(nums, start, end);

			if (pivot < index && pivot != end)
				start = pivot + 1;
			else if (pivot > index)
				end = pivot - 1;
			else
				return nums[pivot];
		}
		return nums[start];
	}

	private static int partion(int[] nums, int start, int end) {
		int pivot = start;
		int temp;

		while (start <= end) {
			while (start <= end && nums[start] <= nums[pivot])
				start++;
			while (start <= end && nums[end] > nums[pivot])
				end--;

			if (start > end)
				break;

			temp = nums[start];
			nums[start] = nums[end];
			nums[end] = temp;
		}

		temp = nums[end];
		nums[end] = nums[pivot];
		nums[pivot] = temp;

		return end;
	}

	private static void print(int[] array, int start, int step, int max) {

		int[] tempArray = new int[1_000_000];

		long startTime = 0;

		for (int i = start; i <= max; i += step) {
			
			for (int j = 0; j < 1_000_000; j++) {
				tempArray[j] = array[j];
			}

			if (i == 1_000_000 || i == start)
				startTime = System.nanoTime();

			System.out.println(findKthLargest(tempArray, i));

			if (i == 1_000_000 || i == start)
				System.out.println("time: " + (System.nanoTime() - startTime));

		}

	}

	public static void main(String[] args) throws IOException {
		int[] array = new int[1_000_000];
		Random random = new Random();

		for (int i = 0; i < 1_000_000; i++) {
			array[i] = random.nextInt(20_000_000);
		}

		print(array, 10_000, 10_000, 50_000);
		print(array, 50_000, 50_000, 500_000);
		print(array, 500_000, 100_000, 1_000_000);

	}

}
