package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	
	private static void quickSort(String[] S, int a, int b) {
		if (a >= b)
			return; 

		int left = a;
		int right = b - 1;
		int pivot = S[b].length();
		String swap;
		
		while (left <= right) { 
			
			while (left <= right && S[left].length() < pivot)
				left++;
			
			while (left <= right && S[right].length() > pivot)
				right--;
			if (left <= right) {
				
				swap = S[left];
				S[left] = S[right];
				S[right] = swap;
				left++;
				right--;
			}
		} 
		swap = S[left];
		S[left] = S[b];
		S[b] = swap;
		
		quickSort(S, a, left - 1);
		quickSort(S, left + 1, b);
	}


	public static void main(String[] args) throws FileNotFoundException {
		Scanner scaner = new Scanner(new File("resources/text"));
		String[] words = { "" };
		String text = scaner.useDelimiter("\\Z").next();
		scaner.close();
		text = text.toLowerCase();

		Pattern pt = Pattern.compile("[^a-zA-Z ]");
		Matcher match = pt.matcher(text);
		
		while (match.find()) {
			String s = match.group();
			text = text.replaceAll("\\" + s, "");
		}
		
		words = text.split(" ");
		
		quickSort(words, 0, 199);
		for(int i = 0; i<= 199; i++)
		{
			System.out.println(words[i]);
		}
	}

}
