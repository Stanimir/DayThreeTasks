package main;

import java.util.Random;

public class Main {
	private static int[] array;
	private static int[] tempMergArr;
	private static int length;

	private static void quickSort(int[] S, int a, int b) {
		if (a >= b)
			return;

		int left = a;
		int right = b - 1;
		int pivot = S[b];
		int swap;

		while (left <= right) {

			while (left <= right && S[left] < pivot)
				left++;

			while (left <= right && S[right] > pivot)
				right--;

			if (left <= right) {
				swap = S[left];
				S[left] = S[right];
				S[right] = swap;
				left++;
				right--;
			}

		}

		swap = S[left];
		S[left] = S[b];
		S[b] = swap;

		quickSort(S, a, left - 1);
		quickSort(S, left + 1, b);
	}

	public static void mergeSort(int inputArr[]) {
		array = inputArr;
		length = inputArr.length;
		tempMergArr = new int[length];
		mergeSort(0, length - 1);
	}

	private static void mergeSort(int lowerIndex, int higherIndex) {

		if (lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;

			mergeSort(lowerIndex, middle);

			mergeSort(middle + 1, higherIndex);

			mergeParts(lowerIndex, middle, higherIndex);
		}
	}

	private static void mergeParts(int lowerIndex, int middle, int higherIndex) {

		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempMergArr[i] = array[i];
		}
		int i = lowerIndex;
		int j = middle + 1;
		int k = lowerIndex;
		while (i <= middle && j <= higherIndex) {
			if (tempMergArr[i] <= tempMergArr[j]) {
				array[k] = tempMergArr[i];
				i++;
			} else {
				array[k] = tempMergArr[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			array[k] = tempMergArr[i];
			k++;
			i++;
		}

	}

	private static void insertSort(int[] array) {
		int swap;
		int place;

		for (int i = 0; i < array.length; i++) {
			place = i;

			for (int j = i + 1; j < array.length; j++) {

				if (array[j] < array[place]) {
					place = j;
				}

			}

			if (place != i) {
				swap = array[i];
				array[i] = array[place];
				array[place] = swap;
			}

		}

	}

	public static void main(String[] args) {
		int[] merge = new int[100_000];
		int[] quick = new int[100_000];
		int[] insert = new int[100_000];
		int tempRandom;
		Random random = new Random();

		for (int i = 0; i < 100_000; i++) {
			tempRandom = random.nextInt(20_000_000);
			merge[i] = tempRandom;
			insert[i] = tempRandom;
			quick[i] = tempRandom;
		}

		long start = System.nanoTime();
		mergeSort(merge);
		System.out.println(System.nanoTime() - start);

		start = System.nanoTime();
		quickSort(quick, 0, 99999);
		System.out.println(System.nanoTime() - start);

		start = System.nanoTime();
		insertSort(insert);
		System.out.println(System.nanoTime() - start);
	}

}
